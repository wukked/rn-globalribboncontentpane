﻿/****************************
 * 
 * Written by:  Kevin Wu @ Accenture
 * Date:        9/7/13
 * Purpose:     Adds a ribbon and button that opens a new custom workspace content pane
 * 
 ****************************/

using System;
using System.Collections.Generic;
using RightNow.AddIns.AddInViews;
using System.AddIn;
using System.Windows.Forms;

namespace GlobalRibbonContentPane
{

    /// <summary>
    /// Ribbon tab object 
    /// </summary>

    [AddIn("Ribbon Tab AddIn", Version = "1.0.0.0")]
    public class WuGlobalRibbonTab : IGlobalRibbonTab
    {
        public string KeyTips
        {
            get { return "Wu Tips"; }
        }

        public int Order
        {
            get { return 0; }
        }

        public string Text
        {
            get { return "Wu Tab"; }
        }

        public bool Visible
        {
            get { return true; }
        }

        public event EventHandler VisibleChanged;

        public bool Initialize(IGlobalContext context)
        {
            return true;
        }
    }

    /// <summary>
    /// Ribbon group object 
    /// </summary>
    [AddIn("Ribbon Group AddIn", Version = "1.0.0.0")]
    public class WuGlobalRibbonGroup : IGlobalRibbonGroup
    {
        public int Order
        {
            get { return 0; }
        }

        public string TabName
        {
            get { return typeof(WuGlobalRibbonTab).FullName; }
        }

        public string Text
        {
            get { return "Wu Group"; }
        }

        public bool Visible
        {
            get { return true; }
        }

        public event EventHandler VisibleChanged;

        public bool Initialize(IGlobalContext context)
        {
            return true;
        }
    }

    /// <summary>
    /// Ribbon button object 
    /// </summary>
    [AddIn("Ribbon Button AddIn", Version = "1.0.0.0")]
    public class WuGlobalRibbonButton : IGlobalRibbonButton
    {
        public IGlobalContext globalContext;

        public void Click()
        {
            //call automation context to open a new WuContentPane!
            //check if content pane currently exists, if not open new pane, if it exists find and focus
            if( !globalContext.AutomationContext.IsEditorOpen(typeof(WuGlobalRibbonTab).FullName) )
            {
                globalContext.AutomationContext.OpenEditor(new WuContentPane());
            }
            else 
            { 
                globalContext.AutomationContext.FindAndFocus( typeof(WuGlobalRibbonTab).FullName);
            }
        }

        public bool Enabled
        {
            get { return true; }
        }

        public event EventHandler EnabledChanged;

        public string GroupName
        {
            get { return typeof(WuGlobalRibbonGroup).FullName; }
        }

        public System.Drawing.Image Image16
        {
            get;
            set;
        }

        public System.Drawing.Image Image32
        {
            get;
            set;
        }

        public System.Windows.Forms.Keys Shortcut
        {
            get { return new Keys(); }
        }

        public string Tooltip
        {
            get { return "Wu Button Tooltip"; }
        }


        public string KeyTips
        {
            get { return "Wu Button Keytip"; }
        }

        public int Order
        {
            get { return 0; }
        }

        public string Text
        {
            get { return "Wu Button Text"; }
        }

        public bool Visible
        {
            get { return true; }
        }

        public event EventHandler VisibleChanged;

        public bool Initialize(IGlobalContext context)
        {
            this.globalContext = context;
            return true;
        }
    }
    
    /// <summary>
    /// New custom workspace/editor/content pane to display anything I want
    /// </summary>
    public class WuContentPane : IContentPaneControl
    {
        Control pane = new WuPane();

        //Before close event
        public bool BeforeClose()
        {
            return true;
        }

        //Close event
        public void Closed()
        {
            
        }

        //Panel icon currently set to none
        public System.Drawing.Image Image16
        {
            get;
            set;
        }

        // Summary:
        //     A list of buttons to add to the ribbon when the control is visible in the
        //     content pane.
        public IList<IEditorRibbonButton> RibbonButtons
        {
            get;
            set;
        }

        public string Text
        {
            get { return "Wu Pane"; }
        }

        public string UniqueID
        {
            get { return "WuPane1"; }
        }

        // Summary:
        //     Return the add-in as a control - typically implementation can just be return
        //     this;
        //
        // Returns:
        //     The add-in as a Control
        public Control GetControl()
        {
            return pane;
        }
    }
}
