﻿namespace GlobalRibbonContentPane
{
    partial class WuPane
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.helloWorldLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // helloWorldLabel
            // 
            this.helloWorldLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.helloWorldLabel.Location = new System.Drawing.Point(0, 0);
            this.helloWorldLabel.Name = "helloWorldLabel";
            this.helloWorldLabel.Size = new System.Drawing.Size(150, 150);
            this.helloWorldLabel.TabIndex = 0;
            this.helloWorldLabel.Text = "Hello World";
            this.helloWorldLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // WuPane
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.helloWorldLabel);
            this.Name = "WuPane";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label helloWorldLabel;
    }
}
